FROM registry.fedoraproject.org/fedora:latest

ENV NAME=flatpak-remote VERSION=stable

LABEL name="$NAME" \
      version="$VERSION" \
      usage="This image is meant to be used for creating and publishing Flatpak images" \
      summary="Push a Flatpak application to an OCI remote"

# Install and update packages
RUN dnf update -y
RUN dnf install -y flatpak flatpak-builder openssh-clients jq skopeo git docker
